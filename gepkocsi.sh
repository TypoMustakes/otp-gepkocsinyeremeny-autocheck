#!/bin/sh

declare -a nyeremenyek
for azonosito in $(cat azonositok.dat)
do
	json_data=$(curl https://www.otpbank.hu/apps/composite/api/carsweepstakes/check/"$azonosito" | jq)
	nyeremeny=$(echo "$json_data" | jq '.sweepstakes[].carType')

	if [ -z "$nyeremeny" ]; then
		continue
	else
		notify-send -u critical "Nyertél ezzel: ${azonosito}; Nyereményed: ${nyeremeny}"
		nyeremenyek+="$nyeremeny"
	fi
done

if [ ${#nyeremenyek[@]} == 0 ]; then
	notify-send "Ma sem nyertél ingyen kocsit ://"
fi

exit 1

### otp-gepkocsinyeremeny-autocheck
### Copyright (C) 2022  Miskolczi Richárd
### 
### This program is free software; you can redistribute it and/or modify
### it under the terms of the GNU General Public License as published by
### the Free Software Foundation; either version 2 of the License, or
### (at your option) any later version.
### 
### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
### GNU General Public License for more details.
### 
### You should have received a copy of the GNU General Public License along
### with this program; if not, write to the Free Software Foundation, Inc.,
### 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
###
### Full license: [project-root]/LICENSE
### Contact: <miskolczi.richard@proton.me>
